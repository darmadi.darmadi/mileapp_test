// problem #6.a

let a = 3;
let b = 5;

[a, b] = [b, a];

console.log(a, b);


// problem #6.b

function findMissingNumbers(numbers = []) {
    let checker = [];
    let results = [];
  
    numbers.map(num => checker[num] = true);
    
    for (let i=1; i <= 100; i++) {
        !checker[i] && results.push(i)
    }
    
    return results;
}

numbers1 = [1, 2, 4, 5, 6, 7, 8, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100];

console.log(findMissingNumbers(numbers1));


// problem #6.c

function findMoreThanOne(numbers = []) {
    let checker = [];
    let results = [];

    numbers.map(num => {
        if (!checker[num]) {
            checker[num] = true;
        } else {
            !results.includes(num) && results.push(num);
        }
    })

    return results;
}

numbers2 = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 25, 25, 25, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 34, 34, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 83, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 92, 93, 94, 95, 96, 97, 98, 99, 100];

console.log(findMoreThanOne(numbers2))


// problem #6.d

array_code = ["1.", "1.1.", "1.2.", "1.3.", "1.4.", "1.1.1.", "1.1.2.", "1.1.3.", "1.2.1.", "1.2.2.", "1.3.1.", "1.3.2.", "1.3.3.", "1.3.4.", "1.4.1.", "1.4.3."];

function arrayToObject(param) {
    let object_code = {}
    
    param.map(string => {
    	let [first, second, third] = string.split('.');
      
      if (first && !object_code[first]) {
        object_code[first] = {};
      }
      
      if (second && !object_code[first][second]) {
        object_code[first][second] = {};
      }
      
      if (third) {
        object_code[first][second][third] = string;
      }
    })
    
    return JSON.stringify(object_code);
}

console.log(arrayToObject(array_code))