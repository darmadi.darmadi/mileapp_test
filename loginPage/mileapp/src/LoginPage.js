import React, { useState } from 'react';
import Header from './Header';
import Footer from './Footer';
import './LoginPage.css';

function LoginPage() {
  let [language, setLanguage] = useState('eng');

  let callback = lang => setLanguage(lang);

  return (
    <div id="login-page">
      <Header language={language} callback={callback} />
      <main>
        <form action="" className="login-form">
          <h1>
            {
              language === 'eng'?
              "Your one stop platform to manage all of your filed service management"
              :
              "Platform Anda untuk mengelola semua manajemen layanan di lapangan"
            }
          </h1>
          <div className="input-container">
            <div className="input-group">
              <input 
                type="text" 
                className="required" 
                placeholder={language === 'eng' ? "Enter your organization's name" : "Masukkan nama organisasi Anda"} 
                autofocus required />
            </div>
            <div className="input-group">
              <input type="submit" value={language === 'eng' ? "Login" : "Masuk"} className="button-primary" />
            </div>
          </div>
          {
            language === 'eng'?
            <p className="form-footer">Not registered yet? <a href='#'>Contact us</a> for more info</p>
            :
            <p className="form-footer">Belum terdaftar? <a href='#'>Hubungi kami</a> untuk info lebih lanjut</p>
          }
        </form>
      </main>
      <Footer language={language} />
    </div>
  )
}

export default LoginPage;