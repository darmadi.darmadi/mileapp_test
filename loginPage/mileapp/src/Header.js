import logo from './images/milleapp_logo.png';
import iconEnglish from './images/flag_america.png';
import iconIndonesia from './images/flag_indonesia.png';
import './Header.css';

function Header({language, callback}) {  
  return (
    <header>
      <a href="#" className="home-button">
        <figure className="mileapp-logo">
          <img src={logo} alt="mileapp" />
        </figure>
      </a>
      <div className="button-group">
        <button 
          className={language === 'eng' ? "active button-language" : "button-language"}
          onClick={() => callback('eng')}>
          <figure className="flag-icon">
            <img src={iconEnglish} alt="English" />
          </figure>
        </button>
        <button className={language === 'ind' ? "active button-language" : "button-language"}
          onClick={() => callback('ind')}>
          <figure className="flag-icon">
            <img src={iconIndonesia} alt="Indonesia" />
          </figure>
        </button>
      </div>
    </header>
  )
}

export default Header;