import './Footer.css';

function Footer({language}) {
  return (
    <footer>
      <p>
        {language === 'eng' ? "Call Us Now" : "Hubungi Kami"}: +62 812-1133-5608
      </p>
      <p>
        &copy; Copyright 2019 PT. Paket Informasi Digital. All Rights Reserved
      </p>
    </footer>
  )
}

export default Footer;